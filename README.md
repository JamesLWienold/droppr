<img src="https://gitlab.com/hoppr/droppr/-/raw/dev/media/Droppr-Dragon-Wordmark.png" />
<br />
<br />

# What is Droppr?

Droppr is a command-line tool to facilitate the unpacking of bundles created by [Hoppr](https://hoppr.dev).

Droppr is written in the "Go" language, to produce a single executable, simplifying installation and usage.

To learn more about Droppr, check out the resources below:
- **Documentation**: <https://hoppr.dev/docs/next/droppr/droppr-splash>
- **Source Code**: <https://gitlab.com/hoppr/droppr>

# Getting Started

## Installation

Go to [Droppr Releases](https://gitlab.com/hoppr/droppr/-/releases) and download the appropriate binary for your system. 

## Usage

See [Using Droppr](https://hoppr.dev/docs/next/category/using-droppr) for more information.

# Join Us

We're completely open source, [MIT licensed](LICENSE), and we welcome community contributions!