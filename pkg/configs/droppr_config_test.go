/*
 *  File: droppr_config_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package configs

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPackageType_FillInDefaults(t *testing.T) {

	testCases := map[string]struct {
		testPackage        *PackageType
		expectedTargetType string
		expectedError      error
	}{
		// Tests that Target_Type defaults to "file_system"
		"file system": {
			&PackageType{
				Purl_Type:     "maven",
				File_System:   &FileSystemTargetType{Target_Directory: "/opt/blah"},
				Local_Install: nil,
				Nexus:         nil,
				OCI_Registry:  nil,
			},
			"file_system",
			nil,
		},

		// Tests that Target_Type defaults to "local_install"
		"local install": {
			&PackageType{
				Purl_Type:     "git",
				File_System:   nil,
				Local_Install: &LocalInstallTargetType{Target_Directory: "/foo/bar"},
				Nexus:         nil,
				OCI_Registry:  nil,
			},
			"local_install",
			nil,
		},

		// Tests that it is not an error to have 'target_type: local' , and not have 'local_install:' set
		"local default": {
			&PackageType{
				Purl_Type:     "git",
				Target_Type:   "local",
				File_System:   nil,
				Local_Install: nil,
				Nexus:         nil,
				OCI_Registry:  nil,
			},
			"local",
			nil,
		},

		// Tests that Target_Type defaults to "nexus" when a 'nexus: ' is set in the config
		"nexus": {
			&PackageType{Purl_Type: "docker",
				File_System:   nil,
				Local_Install: nil,
				Nexus: &NexusTargetType{
					Api_Url:   "https://my-nexus.com:8081",
					Repo_Name: "my-bodacious-repo",
				},
				OCI_Registry: nil,
			},
			"nexus",
			nil,
		},

		// Tests that Target_Type defaults to "oci_registry" when a 'oci_registry: ' is set in the config
		"oci registry": {
			&PackageType{Purl_Type: "helm",
				File_System:   nil,
				Local_Install: nil,
				Nexus:         nil,
				OCI_Registry: &OciRegistryTargetType{
					Host:    "harbor.io:5150",
					Project: "my_test_project",
					Tags:    []string{"latest", "version.1.0.0"},
				},
			},
			"oci_registry",
			nil,
		},

		// Tests that an error is given when target_type cannot be determined
		"unknown target type": {
			&PackageType{Purl_Type: "helm",
				File_System:   nil,
				Local_Install: nil,
				Nexus:         nil,
			},
			"",
			fmt.Errorf("unable to determine target type for purl type helm"),
		},

		// Tests that an error is given when more than one target type structure is in the config
		"filesys and nexus": {
			&PackageType{Purl_Type: "npm",
				File_System: &FileSystemTargetType{
					Target_Directory: "/hello/newman",
				},
				Nexus: &NexusTargetType{
					Api_Url:   "http://my-nexus.com",
					Repo_Name: "yadda-yadda-yadda",
				},
			},
			"file_system",
			fmt.Errorf("target type mismatch: nexus vs file_system"),
		},

		// Tests that an error is given when more than one target type structure is in the config
		"filesys and local": {
			&PackageType{
				Purl_Type: "npm",
				File_System: &FileSystemTargetType{
					Target_Directory: "larry",
				},
				Local_Install: &LocalInstallTargetType{
					Target_Directory: "curly",
				},
				Nexus: nil,
			},
			"file_system",
			fmt.Errorf("target type mismatch: local_install vs file_system"),
		},

		// target type mismatch
		"filesys nexus mismatch": {
			&PackageType{
				Purl_Type:   "npm",
				Target_Type: "nexus",
				File_System: &FileSystemTargetType{
					Target_Directory: "moe",
				},
				Local_Install: nil,
				Nexus:         nil,
			},
			"nexus",
			fmt.Errorf("target type mismatch: file_system vs nexus"),
		},

		// another target type mismatch combination
		"nexus vs oci_registry mismatch": {
			&PackageType{
				Purl_Type:   "helm",
				Target_Type: "oci_registry",
				Nexus: &NexusTargetType{
					Api_Url:   "http://my-nexus-foo.com",
					Repo_Name: "bar",
				},
				Local_Install: nil,
				File_System:   nil,
			},
			"oci_registry",
			fmt.Errorf("target type mismatch: nexus vs oci_registry"),
		},

		// Tests that the nexus struct is checked for missing details
		"missing target type structs": {
			&PackageType{
				Purl_Type:   "apt",
				Target_Type: "nexus",
			},
			"nexus",
			fmt.Errorf("missing details for target_type: nexus for purl type: apt"),
		},
	}

	for key, tc := range testCases {
		err := tc.testPackage.FillInDefaults()
		assert.Equal(t, tc.testPackage.Target_Type, tc.expectedTargetType, "Target Type mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(err), fmt.Sprint(tc.expectedError), "Error mismatch, Test Case: "+key)
	}

}

func TestPackageType_Clone(t *testing.T) {
	testCases := map[string]struct {
		testPackage            *PackageType
		filesysTargetDirChange string
		localTargetDirChange   string
		localPackageChange     string
		nexusRepoChange        string
		ociProjectChange       string
	}{
		"filesystem": {
			&PackageType{
				Purl_Type:   "maven",
				Target_Type: "filesys",
				File_System: &FileSystemTargetType{Target_Directory: "/opt/blah"},
			},
			"/opt/yadda",
			"",
			"",
			"",
			"",
		},
		"local": {
			&PackageType{
				Purl_Type:     "maven",
				Target_Type:   "local",
				Local_Install: &LocalInstallTargetType{Target_Directory: "local_dive"},
			},
			"",
			"other_local_hangout",
			"",
			"",
			"",
		},
		"local_cmd": {
			&PackageType{
				Purl_Type:   "maven",
				Target_Type: "local",
				Local_Install: &LocalInstallTargetType{
					Package_Manager_Command: []string{"pip3"},
				},
			},
			"",
			"",
			"dnf",
			"",
			"",
		},

		"nexus": {
			&PackageType{
				Purl_Type:   "maven",
				Target_Type: "nexus",
				Nexus: &NexusTargetType{
					Api_Url:   "https://my-nexus.com:8081",
					Repo_Name: "my-repo-name",
				},
			},
			"",
			"",
			"",
			"your-repo-name",
			"",
		},

		"oci": {
			&PackageType{
				Purl_Type:   "docker",
				Target_Type: "oci_registry",
				OCI_Registry: &OciRegistryTargetType{
					Host:     "contain-o-rama.io",
					Project:  "my-old-project-name",
					Tags:     nil,
					Insecure: true,
				},
			},
			"",
			"",
			"",
			"",
			"my-new-project-name",
		},
	}

	for key, tc := range testCases {
		clone := tc.testPackage.Clone()
		cloneWithMod := tc.testPackage.Clone()
		numCheckedFields := 0

		// Modify the clone - which is supposed to be a deep copy,
		// and check that the original did not change
		if tc.filesysTargetDirChange != "" {
			cloneWithMod.File_System.Target_Directory = tc.filesysTargetDirChange
		}
		if tc.localTargetDirChange != "" {
			cloneWithMod.Local_Install.Target_Directory = tc.localTargetDirChange
		}
		if tc.localPackageChange != "" {
			cloneWithMod.Local_Install.Package_Manager_Command[0] = tc.localPackageChange
		}
		if tc.nexusRepoChange != "" {
			cloneWithMod.Nexus.Repo_Name = tc.nexusRepoChange
		}
		if tc.ociProjectChange != "" {
			cloneWithMod.OCI_Registry.Project = tc.ociProjectChange
		}

		origRef := reflect.ValueOf(*tc.testPackage)

		// check non-pointer fields for equality
		for i := 0; i < origRef.NumField(); i++ {
			fieldName := reflect.TypeOf(*tc.testPackage).Field(i).Name
			q := reflect.ValueOf(*tc.testPackage).FieldByName(fieldName).Kind()
			origValue := reflect.ValueOf(*tc.testPackage).FieldByName(fieldName).Interface()
			cloneValue := reflect.ValueOf(&clone).Elem().FieldByName(fieldName).Interface()

			if q != reflect.Ptr {
				assert.Equal(t, origValue, cloneValue, "error field "+fieldName+" not equal in tc: "+key)
				numCheckedFields++
			}
		}

		if tc.testPackage.Nexus != nil {
			assert.True(t, clone.Nexus != nil)
			assert.Equal(t, *clone.Nexus, *tc.testPackage.Nexus, "error field not cloned in tc: "+key)
			assert.NotEqual(t, *cloneWithMod.Nexus, *tc.testPackage.Nexus, "error modification of clone changed original: "+key)
		} else {
			assert.True(t, clone.Nexus == nil)
		}
		numCheckedFields++

		if tc.testPackage.File_System != nil {
			assert.True(t, clone.File_System != nil)
			assert.Equal(t, *clone.File_System, *tc.testPackage.File_System, "error field not cloned in tc: "+key)
			assert.NotEqual(t, *cloneWithMod.File_System, *tc.testPackage.File_System, "error modification of clone changed original: "+key)

		} else {
			assert.True(t, clone.File_System == nil)
		}
		numCheckedFields++

		if tc.testPackage.Local_Install != nil {
			assert.True(t, clone.Local_Install != nil)
			assert.Equal(t, *clone.Local_Install, *tc.testPackage.Local_Install, "error field not cloned in tc: "+key)
			assert.NotEqual(t, *cloneWithMod.Local_Install, *tc.testPackage.Local_Install, "error modification of clone changed original: "+key)

		} else {
			assert.True(t, clone.Local_Install == nil)
		}
		numCheckedFields++

		if tc.testPackage.OCI_Registry != nil {
			assert.True(t, clone.OCI_Registry != nil)
			assert.Equal(t, *clone.OCI_Registry, *tc.testPackage.OCI_Registry, "error field not cloned in tc: "+key)
			assert.NotEqual(t, *cloneWithMod.OCI_Registry, *tc.testPackage.OCI_Registry, "error modification of clone changed original: "+key)

		} else {
			assert.True(t, clone.OCI_Registry == nil)
		}
		numCheckedFields++

		// check that each field in PackageType was checked
		numFields := reflect.TypeOf(PackageType{}).NumField()
		assert.Equal(t, numCheckedFields, numFields, "error clone test didn't check all fields")

	}

}

func TestFillInConfigDefaults(t *testing.T) {
	testCases := map[string]struct {
		testConfig    DropprConfig
		expectedError error
	}{
		"good config": {
			DropprConfig{
				Repos: []PackageType{
					PackageType{
						Purl_Type:   "apt",
						Target_Type: "nexus",
						Nexus: &NexusTargetType{
							Api_Url:   "http://my-nexus.com",
							Repo_Name: "test-repo",
						},
					},
					PackageType{
						Purl_Type:   "pypi",
						Target_Type: "filesys",
						File_System: &FileSystemTargetType{
							Target_Directory: "test_dir",
						},
					},
				},
			},
			nil,
		},
		"too many structures": {
			DropprConfig{
				Repos: []PackageType{
					PackageType{
						Purl_Type: "apt",
						Nexus: &NexusTargetType{
							Api_Url:   "http://my-nexus.com",
							Repo_Name: "test-repo",
						},
						File_System: &FileSystemTargetType{
							Target_Directory: "test_dir",
						},
					},
				},
			},
			fmt.Errorf("fill defaults for configuration(s)"),
		},
	}

	for key, tc := range testCases {
		err := tc.testConfig.FillInConfigDefaults()
		assert.Equal(t, fmt.Sprint(err), fmt.Sprint(tc.expectedError), "Error mismatch, Test Case: "+key)
	}

}
