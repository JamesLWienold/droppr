/*
 *  File: helm_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"testing/fstest"

	"helm.sh/helm/v3/pkg/pusher"
	"helm.sh/helm/v3/pkg/registry"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestHelmInstallNexus(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		fileNames []string
		uploadRc  []int
		openError bool
		expected  Result
	}{
		"good": {
			[]string{"alpha_1.2.3.tgz", "beta_4.5.tgz", "gamma_6.7.8.tgz"},
			[]int{200, 200, 200},
			false,
			Result{true, placeholderComp, ""},
		},
		"dir read error": {
			[]string{"alpha_1.2.3.tgz", "ERRbeta_4.5.tgz", "gamma_6.7.8.tgz"},
			[]int{200, 200, 200},
			false,
			Result{false, placeholderComp, "Mock Directory Read Error"},
		},
		"upload failure": {
			[]string{"alpha_1.2.3.tgz", "beta_4.5.tgz", "gamma_6.7.8.tgz"},
			[]int{200, 200, 400},
			false,
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
		"read error": {
			[]string{"alpha_1.2.3.tgz", "beta_4.5.tgz", "gamma_6.7.8.tgz"},
			[]int{200, 200, 200},
			true,
			Result{false, placeholderComp, "Mock Open Error"},
		},
		"check repo error": {
			[]string{"alpha_1.2.3.tgz", "beta_4.5.tgz", "gamma_6.7.8.tgz"},
			[]int{400, 200, 200},
			false,
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
	}

	for key, tc := range testCases {
		serverCallIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[serverCallIndex])
			serverCallIndex++
		}))
		defer server.Close()

		dist := helmDist{*NewBaseDist("helm", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent("pkg:helm/name/space/testHelm@1.2.3")

		testNexus := nexus.Server{Url: server.URL}

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name := range mockFileSys {
				if strings.HasPrefix(name, "ERR") {
					return content, fmt.Errorf("Mock Directory Read Error")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}

		result := dist.installNexus(comp, testNexus, "test-helm-repo", mockReadDir, mockOpen)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}

func TestHelmCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-helm-repo\", \"format\": \"helm\", \"type\": \"hosted\"}]",
			200,
			nil,
		},

		"invalid repo": {
			200,
			"[{\"name\": \"test-helm-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test-helm-repo has format 'pypi', format 'helm' requested"),
		},
		"create new repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"helm\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"helm\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"helm\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
		"validate repo error": {
			200,
			"[{\"name\": \"test-helm-repo\", \"format\": \"helm\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}
		}))
		defer server.Close()

		dist := helmDist{*NewBaseDist("helm", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}

		err := dist.CheckNexusRepository(testNexus, "test-helm-repo")

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestHelmInstallOCI(t *testing.T) {
	placeholderComp := testComponent("testOCI")
	testCases := map[string]struct {
		tryLogin     bool
		dirWork      bool
		loginWork    bool
		loginErrStr  string
		pushWork     bool
		pusherErrStr string
		expected     bool //Success state
		errorStr     string
	}{
		"all_good": {
			true,
			true,
			true,
			"",
			true,
			"",
			true,
			"",
		},
		"login error": {
			true,
			true,
			false,
			"Login Bad",
			false, //who cares
			"stuff",
			false,
			"Error Logging into Repo Client ",
		},
		"push failure": {
			false,
			true,
			true,
			"stuff",
			false,
			"Push Bad",
			false,
			"Error Pushing base_directory/type/http%3A%2F%2Fmy-repo/the/collection/dir/Grumpy.tgz to oci://whocares:5000/projectcares ",
		},
		"dir failure": {
			true,
			false,
			true,
			"stuff",
			false,
			"Push Bad",
			false,
			"Error Opening Temp Bundle Directory ",
		},
	}

	for key, tc := range testCases {

		my_files := []string{"Grumpy.tgz"}

		mockFileSys := fstest.MapFS{}
		for _, name := range my_files {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name := range mockFileSys {
				if !tc.dirWork {
					return content, fmt.Errorf("")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		mockLogin := func(host string, options ...registry.LoginOption) error {
			if tc.loginWork {
				return nil
			} else {

				return errors.New("")
			}
		}

		mockPusher := func(chartRef string, url string, options ...pusher.Option) error {
			if tc.pushWork {
				return nil
			} else {
				return errors.New("")
			}
		}

		dist := helmDist{*NewBaseDist("helm", testConfig("oci_registry", "loc"), "base_directory")}

		if tc.tryLogin {
			repoConfig := &configs.PackageType{Purl_Type: "helm", Target_Type: "oci_registry", Username: "me", Password: "itasecret",
				OCI_Registry: &configs.OciRegistryTargetType{Host: "oci://whocares:5000", Project: "projectcares", Tags: []string{"latest"}}}
			actual := dist.installOciRegistry(placeholderComp, repoConfig, mockReadDir, mockPusher, mockLogin)
			assert.Equal(t, tc.expected, actual.Success, "Error, Test Case: "+key)
			if tc.expected == false {
				assert.Equal(t, tc.errorStr, actual.Message, "Error, Test Case: "+key)
			}

		} else {
			repoConfig := &configs.PackageType{Purl_Type: "helm", Target_Type: "oci_registry",
				OCI_Registry: &configs.OciRegistryTargetType{Host: "oci://whocares:5000", Project: "projectcares", Tags: []string{"latest"}}}
			actual := dist.installOciRegistry(placeholderComp, repoConfig, mockReadDir, mockPusher, mockLogin)
			assert.Equal(t, tc.expected, actual.Success, "Error, Test Case: "+key)
			if tc.expected == false {
				assert.Equal(t, tc.errorStr, actual.Message, "Error, Test Case: "+key)
			}
		}

	}

}
