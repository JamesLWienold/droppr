/*
 *  File: distribute.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"io"
	"os"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type mavenDist struct {
	baseDist
}

func (self *mavenDist) InstallLocal(comp cdx.Component) Result {
	return self.installLocal(comp)
}

func (self *mavenDist) installLocal(
	comp cdx.Component,
) Result {

	// Maven local install process from https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	purl := ParsePurl(comp.PackageURL)
	extension := purl.Quatlifiers["type"]
	if extension == "" {
		extension = "tar.gz"
	}

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		return Result{false, comp, fmt.Sprintf("Error installing %s component locally: %s", comp.PackageURL, err)}
	}
	if len(repoConfig.Local_Install.Package_Manager_Command) == 0 || repoConfig.Local_Install.Package_Manager_Command[0] == "" {
		repoConfig.Local_Install.Package_Manager_Command = []string{"mvn"}
	}

	pomFile := dir + string(os.PathSeparator) + purl.Name + "_" + purl.Version + ".pom"

	command := append([]string{}, repoConfig.Local_Install.Package_Manager_Command...)
	command = append(command, "install:install-file", "-DpomFile="+pomFile)

	if extension != "pom" {
		archiveFile := dir + string(os.PathSeparator) + purl.Name + "_" + purl.Version + "." + extension
		command = append(command, "-Dfile="+archiveFile)
	}

	output, err := runCommand(command)
	self.Log().Println("Command output:\n     " + strings.ReplaceAll(output, "\n", "\n     "))
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func (self *mavenDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_maven"
	}

	targetNexus := nexus.Server{
		Url:      repoConfig.Nexus.Api_Url,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoName, utils.OpenFileAsReader)
}

func (self *mavenDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	openFunc func(name string) (io.Reader, error),
) Result {

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	purl := ParsePurl(comp.PackageURL)
	extension := purl.Quatlifiers["type"]
	if extension == "" {
		extension = "tar.gz"
	}

	filename := purl.Name + "_" + purl.Version + "." + extension
	fptr, err := openFunc(dir + string(os.PathSeparator) + filename)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	data := map[string]interface{}{
		"maven2.groupId":          purl.Namespace,
		"maven2.artifactId":       purl.Name,
		"maven2.version":          purl.Version,
		"maven2.asset1.extension": extension,
	}

	err = targetNexus.Upload(filename, "maven2.asset1", fptr, repoName, data)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	// Unless it was a pom-only artifact (in which case the pom would be uploaded above), upload the pom

	if extension != "pom" {
		pomfn := purl.Name + "_" + purl.Version + ".pom"
		pomptr, err := openFunc(dir + string(os.PathSeparator) + pomfn)
		if err != nil {
			return Result{false, comp, err.Error()}
		}

		data["maven2.asset1.extension"] = "pom"

		err = targetNexus.Upload(pomfn, "maven2.asset1", pomptr, repoName, data)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}
}

func (self *mavenDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"maven": map[string]string{
			"versionPolicy": "MIXED",
			"layoutPolicy":  "STRICT",
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "maven2", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "maven", "maven2", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}
