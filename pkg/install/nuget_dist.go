/*
 *  File: nuget_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"io"
	"io/fs"
	"os"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type nugetDist struct {
	baseDist
}

func (self *nugetDist) InstallFilesys(comp cdx.Component) Result {
	return self.installFilesys(comp, os.Stat, os.MkdirAll, utils.CopyFile)
}

func (self *nugetDist) installFilesys(comp cdx.Component, statFunc func(string) (os.FileInfo, error), mkdirFunc func(string, fs.FileMode) error, copyFileFunc func(string, string) error) Result {
	collectionData := getCollectionData(comp)

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error (GetRepoConfig) installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	sourceDir := self.baseDir + string(os.PathSeparator) + collectionData[collectionDirectory]
	destDir :=  repoConfig.File_System.Target_Directory + string(os.PathSeparator) + collectionData[collectionDirectory]

	fileName := GetFileName(comp.PackageURL)
	sourceFilePath := sourceDir + string(os.PathSeparator) + fileName
	destFilePath := destDir + string(os.PathSeparator) + fileName

	var sourceInfo os.FileInfo
	if sourceInfo, err = statFunc(sourceDir); err != nil {
		msg := fmt.Sprintf("Error (Stat) installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	if err = mkdirFunc(destDir, sourceInfo.Mode()); err != nil {
		msg := fmt.Sprintf("Error (Mkdir) installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	err = copyFileFunc(sourceFilePath, destFilePath)
	if err == nil {
		return Result{true, comp, ""}
	}

	msg := fmt.Sprintf("Unable to copy from %s to %s, %s.", sourceFilePath, destFilePath, err)
	self.Log().Error(msg)
	return Result{false, comp, msg}
}


/*
 * Form a file name based on a string conforming to the PackageURL of a cdx.Component
 */
func GetFileName(packageUrl string) string {
	parsed := ParsePurl(packageUrl)
	return fmt.Sprintf("%s_%s.nupkg", parsed.Name, parsed.Version)
}

func (self *nugetDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Errorf(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_nuget"
	}

	targetNexus := nexus.Server{
		Url:      repoConfig.Nexus.Api_Url,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoName, utils.OpenFileAsReader)
}

func (self *nugetDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	openFunc func(name string) (io.Reader, error),
) Result {
	data := map[string]interface{}{}

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	fileName := GetFileName(comp.PackageURL)
	filePath := dir + string(os.PathSeparator) + fileName

	fptr, err := openFunc(filePath)
	if err != nil {
		return Result{false, comp, err.Error()}
	}
	err = targetNexus.Upload(fileName, "nuget.asset", fptr, repoName, data)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func (self *nugetDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"component": map[string]bool{
			"proprietaryComponents": true,
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "nuget", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "nuget", "nuget", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}
