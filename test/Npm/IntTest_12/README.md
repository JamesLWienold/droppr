# Overview
The IntTest_12 directory is derived from [Hoppr IntTest_12](https://gitlab.com/hoppr/hoppr/-/tree/dev/test/integration/IntTest_12?ref_type=heads)
and allows Droppr to have an NPM integration test using purl identifiers that are compliant with the Purl specification.

# Derivation

The primary change is to npm-bom.json (replace "%2F" with "/":
The original purl is

    pkg:npm/%40types%2Fparse-json@4.0.0

The Droppr purl is

    pkg:npm/%40types/parse-json@4.0.0

Referring to the [PURL Specification](https://github.com/package-url/purl-spec/blob/master/PURL-SPECIFICATION.rst) 
there is the statement:

> the '/' used as type/namespace/name and subpath segments separator does not need to and must NOT be percent-encoded

Since the usage in IntTest_12 is as a namespace separator, adjust the purl to be compliant with the specification.

Note that using "%2F" as a namespace separator is ambiguous: it could belong to the filename or to the namespace.
In the case of Hoppr's IntTest_12, the purl for parse-json  is intended as a namespace separator.

There is a discussion of this in closed merge request https://gitlab.com/hoppr/hoppr/-/merge_requests/466

## Idea

One idea is for Droppr to assume the last "%2F" is a namespace separator and parse the purl accordingly, 
but that is going beyond the specification and opens up the problem of handling the case where the user intends to have the
"%2F" as part of the filename.

# Usage 

To form the bundle.tar.gz

    cd droppr
    hopctl bundle -v -l hoppr.log -t test/Npm/IntTest_12/transfer.yml test/Npm/IntTest_12/manifest.yml


The bundle.tar.gz file goes into test/Npm/IntTest_12.
This could then be copied into test/Npm to be part of Droppr's integration test.
