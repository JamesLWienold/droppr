# Arguments
#  1: output directory name

if [ $# -ne 1 ]; then
    echo "Bad arguments"
    exit 2
fi

USER="droppr"
# filename matches .droppr.yml, property apt_signing_keypair_file
PRIVATE_FILE=$1/droppr.private.gpg.key

OUTPUT=$(gpg --batch --passphrase '' --quick-gen-key $USER default default 2>&1)
if [ $? -ne 0 ]; then
    echo $OUTPUT
    exit 1
fi

# Regular expressions to extract the key ID (fingerprint)
REGEX1="/([0-9ABCDEF]+).rev"

KEY_ID=""
if [[ $OUTPUT =~ $REGEX1 ]]; then
    KEY_ID=${BASH_REMATCH[1]}
fi

if [ KEY_ID = "" ]; then
    echo "No key ID found in $OUTPUT"
    exit 1
fi

# form the private key
gpg --batch --yes --armor --output $PRIVATE_FILE --export-secret-key $KEY_ID
